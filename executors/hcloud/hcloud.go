package hcloud

import (
	"context"
	"errors"
	"strconv"

	"gitlab.com/gitlab-org/gitlab-runner/common"
	"gitlab.com/gitlab-org/gitlab-runner/executors"
	"gitlab.com/gitlab-org/gitlab-runner/helpers/ssh"

	"github.com/hetznercloud/hcloud-go/hcloud"
)

type executor struct {
	executors.AbstractExecutor
	sshCommand   ssh.Client
	hcloudClient *hcloud.Client
	server       *hcloud.Server
	imageid      int
}

func (s *executor) createServer(name string, server_type string, image int) (hcloud.ServerCreateResult, error) {

	s.Println("Using Name", name)

	// Get Image
	serverImage, _, err := s.hcloudClient.Image.GetByID(context.Background(), image)
	if err != nil {
		return hcloud.ServerCreateResult{}, errors.New("Could not find hcloud image " + strconv.Itoa(image))
	}

	s.Println("Using Image", serverImage.Name, serverImage.Description)

	// Get Type
	serverType, _, err := s.hcloudClient.ServerType.GetByName(context.Background(), server_type)
	if err != nil {
		return hcloud.ServerCreateResult{}, errors.New("Could not find " + server_type)
	}

	s.Println("Using ServerType", serverType.Name)

	// Create server
	server, _, err := s.hcloudClient.Server.Create(context.Background(), hcloud.ServerCreateOpts{
		Name:       name,
		Image:      serverImage,
		ServerType: serverType,
	})

	if err != nil {
		return hcloud.ServerCreateResult{}, errors.New("Could not create server" + err.Error())
	}

	return server, nil
}

func (s *executor) waitForActionCompletion(action *hcloud.Action) error {
	progress, err := s.hcloudClient.Action.WatchProgress(context.Background(), action)

	oldprogress := -1

	for {
		select {
		case v := <-progress:
			if v != oldprogress {
				oldprogress = v
				s.Println(action.Command, ":", v, "%")
			}

			if v == 100 {
				return nil
			}
		case <-err:
			return errors.New("Error during " + action.Command)
		}
	}
}

func (s *executor) Prepare(options common.ExecutorPrepareOptions) error {

	err := s.AbstractExecutor.Prepare(options)
	if err != nil {
		return err
	}

	if s.Config.SSH == nil {
		return errors.New("Missing SSH config")
	}

	if s.Config.HCloud == nil {
		return errors.New("Missing HCloud configuration")
	}

	if s.Config.HCloud.Token == "" {
		return errors.New("Missing Token setting from HCloud configuration")
	}

	if s.Config.HCloud.Image == "" {
		return errors.New("Missing Image setting from HCloud configuration")
	}

	s.imageid, err = strconv.Atoi(s.Config.HCloud.Image)
	if err != nil {
		return errors.New("Image setting shall be numeric")
	}

	if s.Config.HCloud.BaseName == "" {
		return errors.New("Missing BaseName setting from HCloud configuration")
	}

	// if s.Config.HCloud.SSHPort == 0 {}

	s.hcloudClient = hcloud.NewClient(hcloud.WithToken(s.Config.HCloud.Token))
	//if s.hcloudClient != nil {
	//	return errors.New("Could not create hcloud client")
	//}

	s.Println("Creating new VM ...")
	server, err := s.createServer(s.Config.HCloud.BaseName+"-"+s.Build.ProjectUniqueName(), "cx11", s.imageid)
	if err != nil {
		return err
	}

	s.server = server.Server

	s.Println("ID\t", s.server.ID)
	s.Println("NAME\t", s.server.Name)
	s.Println("DATACENTER\t", s.server.Datacenter.Name)

	s.Println("Waiting for VM to get ready")

	err = s.waitForActionCompletion(server.Action)
	if err != nil {
		return err
	}

	for _, act := range server.NextActions {
		s.Println(act.Command)
		err := s.waitForActionCompletion(act)
		if err != nil {
			return err
		}
	}

	s.Debugln(s.server.PublicNet.IPv4.DNSPtr)

	s.Println("Starting SSH command ...")
	s.sshCommand = ssh.Client{
		Config: *s.Config.SSH,
		Stdout: s.Trace,
		Stderr: s.Trace,
	}
	s.sshCommand.Port = "22"
	s.sshCommand.Host = s.server.PublicNet.IPv4.DNSPtr

	s.Debugln("Connecting to SSH server...")
	err = s.sshCommand.Connect()
	if err != nil {
		return err
	}

	return nil
}

func (s *executor) Cleanup() {
	if s.server != nil {
		_, err := s.hcloudClient.Server.Delete(context.Background(), s.server)
		if err != nil {
			s.Println("Could not delete server")
		}
	}
	s.sshCommand.Cleanup()
	s.AbstractExecutor.Cleanup()
}

func (s *executor) Run(cmd common.ExecutorCommand) error {

	err := s.sshCommand.Run(cmd.Context, ssh.Command{
		Command: s.BuildShell.CmdLine,
		Stdin:   cmd.Script,
	})
	if _, ok := err.(*ssh.ExitError); ok {
		err = &common.BuildError{Inner: err}
	}
	return err
}

func (s *executor) Name() string {
	return "hcloud"
}

func init() {
	options := executors.ExecutorOptions{
		DefaultBuildsDir: "builds",
		SharedBuildsDir:  false,
		Shell: common.ShellScriptInfo{
			Shell:         "bash",
			Type:          common.LoginShell,
			RunnerCommand: "gitlab-runner",
		},
		ShowHostname: true,
	}

	creator := func() common.Executor {
		return &executor{
			AbstractExecutor: executors.AbstractExecutor{
				ExecutorOptions: options,
			},
		}
	}

	featuresUpdater := func(features *common.FeaturesInfo) {
		features.Variables = true
	}

	common.RegisterExecutorProvider("hcloud", executors.DefaultExecutorProvider{
		Creator:          creator,
		FeaturesUpdater:  featuresUpdater,
		DefaultShellName: options.Shell.Shell,
	})
}
